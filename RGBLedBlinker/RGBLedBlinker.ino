#define PORT_LED_R    9
#define PORT_LED_G    10
#define PORT_LED_B    11

#define MAX_DUTY      256
#define TOTAL_CYCLES  6

long cycles = 0;

void setup() {}

void loop() {
  setBrightness();
  cycles++;
}

void setBrightness() {
  int cycleNo = (cycles / MAX_DUTY) % TOTAL_CYCLES;
  int dutyPerCycle = cycles % MAX_DUTY;

  switch (cycleNo) {
    case 0:
      analogWrite(PORT_LED_R, dutyPerCycle);
      break;
    case 1:
      analogWrite(PORT_LED_R, MAX_DUTY - dutyPerCycle - 1);
      break;
    case 2:
      analogWrite(PORT_LED_G, dutyPerCycle);
      break;
    case 3:
      analogWrite(PORT_LED_G, MAX_DUTY - dutyPerCycle - 1);
      break;
    case 4:
      analogWrite(PORT_LED_B, dutyPerCycle);
      break;
    case 5:
      analogWrite(PORT_LED_B, MAX_DUTY - dutyPerCycle - 1);
      break;
  }
  delay(1);
}
