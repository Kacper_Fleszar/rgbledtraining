#include "HSVToRGB.h"

#define PORT_LED_R    9
#define PORT_LED_G    10
#define PORT_LED_B    11

#define MAX_HUE 360

int counter = 0;

void setup() {}

void loop() {
  HSVToRGB hsvToRgb(counter%MAX_HUE);
  analogWrite(PORT_LED_R, hsvToRgb.getR());
  analogWrite(PORT_LED_G, hsvToRgb.getG());
  analogWrite(PORT_LED_B, hsvToRgb.getB());
  delay(10);
  counter++;
}
