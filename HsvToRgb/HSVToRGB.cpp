#include "HSVToRGB.h"

HSVToRGB::HSVToRGB(int hue) {
	calculateRGB(hue);
}

int HSVToRGB::getR() {
	return _r;
}

int HSVToRGB::getG() {
	return _g;
}

int HSVToRGB::getB() {
	return _b;
}

void HSVToRGB::calculateRGB(int hue) {
	if (hue >= 0 && hue < 60) {
		_r=255, _g=calculateHue(hue), _b=0;
	} else if (hue >= 60 && hue < 120) {
		_r=calculateHue(hue), _g=255, _b=0;
	} else if (hue >= 120 && hue < 180) {
		_r=0, _g=255, _b=calculateHue(hue);
	} else if (hue >= 180 && hue < 240) {
		_r=0, _g=calculateHue(hue), _b=255;
	} else if (hue >= 240 && hue < 300) {
		_r=calculateHue(hue), _g=0, _b=255;
	} else {
		_r=255, _g=0, _b=calculateHue(hue);
	}
}

float HSVToRGB::calculateHue(int hue) {
	return 255*(1-abs(fmod(hue/60.0, 2)-1));
}
