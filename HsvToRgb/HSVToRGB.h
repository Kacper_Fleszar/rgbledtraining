#ifndef HSVToRGB_h
#define HSVToRGB_h
#include "Arduino.h"

/*
 * Brightness for each colour is in range 0-255
 * Hue range <0,359> degrees
 * Saturation range <0,100> %
 * Value range <0, 100> %
 * RGB range is <0,255>
 */
class HSVToRGB {
	public:
		HSVToRGB(int hue);
		int getR();
		int getG();
		int getB();
	private:
		int _r;
		int _g;
		int _b;
		void calculateRGB(int hue);
		float calculateHue(int hue);
};
#endif
